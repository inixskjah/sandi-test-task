
## Products parser & Shop Example

This project uses:

- spatie/laravel-translatable

Installation:

```
composer install
php artisan:migrate
```

`Notice:` This parses only first 100 products for demonstration purposes