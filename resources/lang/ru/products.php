<?php

return [
    'index' => [
        'button_more' => "Подробнее"
    ],

    'show' => [
        'description'   => "Описание товара",
        'price'         => "Цена",

        'attributes'    => [
            'title'     => "Характеристики",
            'col_name'  => 'Характеристика',
            'col_value' => 'Значение'
        ]
    ]
];