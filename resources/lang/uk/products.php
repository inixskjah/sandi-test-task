<?php

return [
    'index' => [
        'button_more' => "Докладніше"
    ],

    'show' => [
        'description'   => "Опис товару",
        'price'         => "Ціна",

        'attributes'    => [
            'title'     => "Характеристики",
            'col_name'  => 'Характеристика',
            'col_value' => 'Значення'
        ]
    ]
];