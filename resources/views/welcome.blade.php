@extends('layout')

@section('content')
<div class="title m-b-md">
    @lang('common.meta.title')
</div>

<div class="links">
    <a href="{{ route('products.parse') }}">@lang('welcome.button_parse')</a>
    <a href="{{ route('products.index') }}">@lang('welcome.button_list')</a>
</div>

@endsection