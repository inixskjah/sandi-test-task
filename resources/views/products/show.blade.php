@extends('layout')

@section('content')

    <div class="col-md-10 col-md-offset-1">
        <figure class="card card-product">
            <div class="row">
                <div class="col-md-2">
                    <div class="img-wrap"><img src="https://www.chillinoodle.co.uk/skin/frontend/chillinoodle/default/images/catalog/product/placeholder/image.jpg"></div>
                </div>
                <div class="col-md-10">
                    <figcaption class="info-wrap">
                        <h4 class="title">{{ $product->name }}</h4>
                        <p class="desc"><b>@lang('products.show.description'):</b> {!! $product->description !!}</p>
                    </figcaption>
                    <div class="bottom-wrap">
                        <div class="price-wrap h5">
                            <span class="price-new">@lang('products.show.price'): {{ $product->price }} UAH</span>
                        </div> <!-- price-wrap.// -->
                    </div> <!-- bottom-wrap.// -->
                    <figcaption class="info-wrap">
                        <h4>@lang('products.show.attributes.title')</h4>
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">@lang('products.show.attributes.col_name')</th>
                                <th scope="col">@lang('products.show.attributes.col_value')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product->attributes as $attribute)
                            <tr>
                                <td>{{ $attribute->attribute->name }}</td>
                                <td>{{ $attribute->value }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </figcaption>
                </div>
            </div>
        </figure>
    </div> <!-- col // -->

@endsection('content')