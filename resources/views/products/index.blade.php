@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        @foreach($products as $product)

        <div class="col-md-4">
            <figure class="card card-product">
                <div class="img-wrap"><img src="https://www.chillinoodle.co.uk/skin/frontend/chillinoodle/default/images/catalog/product/placeholder/image.jpg"></div>
                <figcaption class="info-wrap">
                    <h4>{{ $product->name }}</h4>
                    <p class="desc">{{ Str::limit($product->description, 100) }}</p>
                </figcaption>
                <div class="bottom-wrap">
                    <a href="{{ route('products.show', [ 'product' => $product ]) }}" class="btn btn-sm btn-primary float-right">@lang('products.index.button_more') >> </a>
                    <div class="price-wrap h5">
                        <span class="price-new">{{ $product->price }} UAH</span>
                    </div> <!-- price-wrap.// -->
                </div> <!-- bottom-wrap.// -->
            </figure>
        </div> <!-- col // -->

        @endforeach
    </div>
    <div class="row float-right">
        {!! $products->links() !!}
    </div>
</div>

@endsection