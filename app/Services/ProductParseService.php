<?php

namespace App\Services;

use App\Product;

class ProductParseService
{
    /**
     * Api service
     *
     * @var SandiApiService
     */
    public $api;

    /**
     * ProductParseService constructor.
     * @param SandiApiService $api
     */
    public function __construct(SandiApiService $api)
    {
        $this->api = $api;
    }

    /**
     * @param int $limit
     * @return \Illuminate\Support\Collection
     */
    public function parseList(int $limit = null)
    {
        $products = $this->api->getProducts();

        return $limit ? $products->take($limit) : $products;
    }
}