<?php

namespace App\Services;


use Illuminate\Support\Facades\Http;

class SandiApiService
{
    /**
     * Sandi API domain
     *
     * @var string
     */
    private $url;

    /**
     * ProductParseService constructor.
     * @param string $api_domain
     */
    public function __construct(string $api_domain)
    {
        $this->url = $api_domain;
    }

    /**
     * Get products list by API
     *
     * @return \Illuminate\Support\Collection
     */
    public function getProducts()
    {
        return $this->get('products');
    }

    /**
     * @param string $path
     * @return \Illuminate\Support\Collection
     */
    private function get(string $path)
    {
        $response = Http::get($this->url . $path);

        return collect($response->json());
    }
}