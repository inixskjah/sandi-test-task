<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Attribute extends Model
{
    use HasTranslations;

    public $translatable = [
        'name'
    ];

    public $fillable = [
        "name"
    ];

    public $timestamps = null;

    public $casts = [
        'name' => 'json'
    ];
}
