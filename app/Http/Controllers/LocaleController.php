<?php

namespace App\Http\Controllers;

class LocaleController extends Controller
{
    public function set(string $locale)
    {
        if (in_array($locale, [
            'ru', 'uk'
        ])) {
            app()->setLocale($locale);
            session()->put('locale', $locale);
        }

        return redirect()->back();
    }
}
