<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\AttributeValue;
use App\Product;
use App\Services\ProductParseService;

class ProductParseController extends Controller
{

    /**
     * @var ProductParseService
     */
    private $parser;

    /**
     * ProductParseController constructor.
     * @param ProductParseService $parser
     */
    public function __construct(ProductParseService $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $products = $this->parser->parseList(100);

        Product::query()->delete();
        Attribute::query()->delete();
        AttributeValue::query()->delete();

        foreach ($products as $product)
        {
            $productModel = new Product([
                "sku" => $product["sku"],
                "price" => $product["price"]
            ]);

            $productModel->setTranslation('name', 'ru', $product["name_ru"]);
            $productModel->setTranslation('name', 'uk', $product["name_uk"]);

            $productModel->setTranslation('description', 'ru', $product["description_ru"]);
            $productModel->setTranslation('description', 'uk', $product["description_uk"]);

            $productModel->save();

            foreach ($product["characteristics"] as $attribute)
            {
                $attributeModel = Attribute::where([
                    'name->ru' => $attribute["name_ru"]
                ])->first() ?? new Attribute();

                $attributeModel->setTranslation('name', 'ru', $attribute["name_ru"]);
                $attributeModel->setTranslation('name', 'uk', $attribute["name_uk"]);

                $attributeModel->save();

                $attributeValue = new AttributeValue([
                    'product_id' => $productModel->id,
                    'attribute_id' => $attributeModel->id
                ]);

                $attributeValue->setTranslation('value', 'ru', $attribute["value_ru"]);
                $attributeValue->setTranslation('value', 'uk', $attribute["value_uk"]);

                $attributeValue->save();
            }
        }

        return redirect()->route('products.index');
    }
}
