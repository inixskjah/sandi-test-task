<?php

namespace App\Http\Middleware;


use Illuminate\Http\Request;

class Locale
{
    public function handle(Request $request, \Closure $next)
    {
        app()->setLocale(
            session()->get('locale', config('translatable.fallback_locale'))
        );
        return $next($request);
    }
}
