<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class AttributeValue extends Model
{
    use HasTranslations;

    public $translatable = [
        'value'
    ];

    public $fillable = [
        "product_id",
        "attribute_id"
    ];

    public $timestamps = null;

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
}
