<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{
    use HasTranslations;

    public $translatable = [
        'name',
        'description'
    ];

    public $fillable = [
        "sku",
        "price"
    ];

    public function attributes()
    {
        return $this->hasMany(AttributeValue::class);
    }
}
