<?php

namespace App\Providers;

use App\Services\ProductParseService;
use App\Services\SandiApiService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SandiApiService::class, function() {
            $api_domain = config('services.sandi.api.domain');
            return new SandiApiService($api_domain);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
